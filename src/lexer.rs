use std::ops::Range;

use logos::Logos;

/// The different atoms of the language.
#[derive(Logos, Debug, Clone, Copy, PartialEq, Eq)]
pub(crate) enum Token {
    #[regex(r"\d+")]
    Number,

    #[token("+")]
    Plus,

    #[token("-")]
    Minus,

    #[token("*")]
    Asterisk,

    #[token("/")]
    Slash,

    #[token(".")]
    Period,

    #[token("=")]
    Equals,

    #[regex(r"\r?\n")]
    Newline,

    #[regex(r"[ \t]+", logos::skip)]
    #[error]
    Error,
}

pub(crate) struct Lexer<'source>(logos::Lexer<'source, Token>);

impl<'source> Iterator for Lexer<'source> {
    type Item = (Token, &'source str, Range<usize>);

    fn next(&mut self) -> Option<Self::Item> {
        let token = self.0.next()?;
        Some((token, self.0.slice(), self.0.span()))
    }
}

/// Convert a Porth program into a Lexer that iterates over its tokens.
pub(crate) fn lex(program: &str) -> Lexer<'_> {
    Lexer(Token::lexer(program))
}

use std::fmt::Display;

#[derive(Debug, PartialEq)]
pub(crate) enum Obj {
    Int(i64),
    Bool(bool),
}

impl Obj {
    pub(crate) fn as_int(&self) -> Option<&i64> {
        if let Self::Int(v) = self {
            Some(v)
        } else {
            None
        }
    }

    pub(crate) fn try_into_int(self) -> Result<i64, Self> {
        if let Self::Int(v) = self {
            Ok(v)
        } else {
            Err(self)
        }
    }
}

impl Display for Obj {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Obj::Int(i) => write!(f, "{}", i),
            Obj::Bool(b) => write!(f, "{}", if *b { 1 } else { 0 }),
        }
    }
}

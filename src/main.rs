use std::fs;
use std::fs::File;
use std::ops::Range;
use std::path::Path;
use std::process::Command;
use std::process::ExitStatus;

use clap::App;
use clap::AppSettings;
use clap::Arg;
use clap::SubCommand;
use obj::Obj;

use crate::lexer::Token;

mod lexer;
mod obj;

enum Op {
    Push(i64),
    Plus,
    Minus,
    Dump,
    Equal,
    Nop,
}

struct SpannedOp {
    op: Op,
    span: Range<(usize, usize)>,
}

/// Call the given command and return its exit status.
fn call(name: &str, args: &[&str]) -> ExitStatus {
    println!("{} {}", name, args.join(" "));
    let result = Command::new(name)
        .args(args)
        .spawn()
        .unwrap()
        .wait()
        .unwrap();
    assert!(result.success());
    result
}

/// Interpret the given filename as a program.
fn simulate_program(program: &[SpannedOp]) {
    let mut stack = Vec::new();
    for op in program {
        match op.op {
            Op::Push(x) => {
                stack.push(Obj::Int(x));
            }
            Op::Plus => {
                let b = stack.pop().unwrap().try_into_int().unwrap();
                let a = stack.pop().unwrap().try_into_int().unwrap();
                stack.push(Obj::Int(a + b));
            }
            Op::Minus => {
                let b = stack.pop().unwrap().try_into_int().unwrap();
                let a = stack.pop().unwrap().try_into_int().unwrap();
                stack.push(Obj::Int(a - b));
            }
            Op::Equal => {
                let b = stack.pop().unwrap();
                let a = stack.pop().unwrap();
                stack.push(Obj::Bool(a == b));
            }
            Op::Dump => {
                println!("{}", stack.last().unwrap());
            }
            Op::Nop => {}
        }
    }
}

/// Compile the given file into assembly. It will be in NASM format.
fn compile_program(program: &[SpannedOp], out_file_path: impl AsRef<Path>) {
    use std::io::Write;

    let mut assembly_file = File::create(out_file_path).unwrap();
    writeln!(assembly_file, "segment .text").unwrap();

    let dump_asm = include_str!("dump.asm");

    writeln!(assembly_file, "{}", dump_asm).unwrap();

    writeln!(assembly_file, "global _start").unwrap();
    writeln!(assembly_file, "_start:").unwrap();
    for op in program {
        match op.op {
            Op::Push(x) => {
                writeln!(assembly_file, "\t;; -- push {} --", x).unwrap();
                writeln!(assembly_file, "\tpush {}", x).unwrap();
            }
            Op::Plus => {
                writeln!(assembly_file, "\t;; -- plus --").unwrap();
                writeln!(assembly_file, "\tpop rax").unwrap();
                writeln!(assembly_file, "\tpop rbx").unwrap();
                writeln!(assembly_file, "\tadd rax, rbx").unwrap();
                writeln!(assembly_file, "\tpush rax").unwrap();
            }
            Op::Minus => {
                writeln!(assembly_file, "\t;; -- minus --").unwrap();
                writeln!(assembly_file, "\tpop rax").unwrap();
                writeln!(assembly_file, "\tpop rbx").unwrap();
                writeln!(assembly_file, "\tsub rbx, rax").unwrap();
                writeln!(assembly_file, "\tpush rbx").unwrap();
            }
            Op::Equal => {
                writeln!(assembly_file, "\t;; -- equal --").unwrap();
                writeln!(assembly_file, "\tmov rcx, 0").unwrap();
                writeln!(assembly_file, "\tmov rdx, 1").unwrap();
                writeln!(assembly_file, "\tpop rax").unwrap();
                writeln!(assembly_file, "\tpop rbx").unwrap();
                writeln!(assembly_file, "\tcmp rax, rbx").unwrap();
                writeln!(assembly_file, "\tcmove rcx, rdx").unwrap();
                writeln!(assembly_file, "\tpush rcx").unwrap();
            }
            Op::Dump => {
                writeln!(assembly_file, "\t;; -- dump --").unwrap();
                writeln!(assembly_file, "\tpop rdi").unwrap();
                writeln!(assembly_file, "\tcall dump").unwrap();
            }
            Op::Nop => {}
        }
    }
    writeln!(assembly_file, "\tmov rax, 60").unwrap();
    writeln!(assembly_file, "\tmov rdi, 0").unwrap();
    writeln!(assembly_file, "\tsyscall").unwrap();
}

fn main() {
    let matches = App::new("porth")
        .version(env!("CARGO_PKG_VERSION"))
        .settings(&[
            AppSettings::SubcommandRequired,
            AppSettings::ColoredHelp,
            AppSettings::VersionlessSubcommands,
        ])
        .subcommand(
            SubCommand::with_name("simulate")
                .alias("sim")
                .about("Simulate the program.")
                .arg(Arg::with_name("FILE").index(1).required(true)),
        )
        .subcommand(
            SubCommand::with_name("compile")
                .alias("com")
                .about("Compile the program.")
                .arg(Arg::with_name("FILE").index(1).required(true))
                .arg(
                    Arg::with_name("run")
                        .short("r")
                        .long("run")
                        .help("Run the compiled program."),
                ),
        )
        .get_matches();

    match matches.subcommand() {
        ("simulate", Some(matches)) => {
            let program = load_program_from_file(matches.value_of("FILE").unwrap());
            simulate_program(&program);
        }
        ("compile", Some(matches)) => {
            let program = load_program_from_file(matches.value_of("FILE").unwrap());
            compile_program(&program, "output.asm");

            call("nasm", &["-f", "elf64", "output.asm", "-o", "output.o"]);
            call("ld", &["-o", "output", "output.o"]);

            if matches.is_present("run") {
                call("./output", &[]);
            }
        }
        _ => unreachable!(),
    }
}

fn load_program_from_file(name: impl AsRef<Path>) -> Vec<SpannedOp> {
    let contents = fs::read_to_string(name).unwrap();
    let mut line = 1;
    let mut line_offset = 0;
    lexer::lex(&contents)
        .map(|(token, slice, span)| {
            let op_span = (line, span.start - line_offset)..(line, span.end - line_offset);
            match token {
                Token::Number => match slice.parse() {
                    Ok(x) => SpannedOp {
                        op: Op::Push(x),
                        span: op_span,
                    },
                    Err(e) => panic!("{} at {}:{}", e, line, span.start),
                },
                Token::Plus => SpannedOp {
                    op: Op::Plus,
                    span: op_span,
                },
                Token::Minus => SpannedOp {
                    op: Op::Minus,
                    span: op_span,
                },
                Token::Period => SpannedOp {
                    op: Op::Dump,
                    span: op_span,
                },
                Token::Equals => SpannedOp {
                    op: Op::Equal,
                    span: op_span,
                },
                Token::Newline => {
                    line += 1;
                    line_offset = span.end;
                    SpannedOp {
                        op: Op::Nop,
                        span: op_span,
                    }
                }
                Token::Error => panic!(
                    "at {}:{}: invalid token '{}'",
                    line,
                    span.start - line_offset + 1,
                    slice
                ),
                _ => panic!(
                    "at {}:{}: unhandled token '{}'",
                    line,
                    span.start - line_offset + 1,
                    slice
                ),
            }
        })
        .collect()
}

#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

#define BUF_CAP 32

void dump(uint64_t x);

int main(void) {
  dump(69420);
  dump(0);
}

void dump(uint64_t x) {
  // max length = 20
  char buf[BUF_CAP];
  size_t buf_sz = 1;
  buf[sizeof(buf) - 1] = '\n';

  while (1) {
    buf[sizeof(buf) - buf_sz - 1] = x % 10 + '0';
    ++buf_sz;
    x /= 10;
    if (x == 0) {
      break;
    }
  }

  write(1, &buf[sizeof(buf) - buf_sz], buf_sz);
}
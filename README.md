# Porth

Porth is Forth but in Python but rewritten in Rust.

Confused yet? Me too.

## Quickstart

```console
cargo run -- sim <FILE>
cargo run -- com <FILE> && ./output
```
